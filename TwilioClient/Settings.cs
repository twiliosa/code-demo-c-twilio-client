﻿namespace TwilioClient
{
    public class Settings
    {
        public static string TwilioAccountSid { get { return ""; } }
        public static string TwilioAuthToken { get { return ""; } }
        public static string ClientAppSid { get { return ""; } }
        public static string OutboundCallerId { get { return ""; } }
        public static string AgentId { get { return ""; } }
    }
}