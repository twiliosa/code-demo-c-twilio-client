﻿using System.Web.Mvc;
using Twilio.TwiML;
using Twilio.TwiML.Mvc;

namespace TwilioClient.Controllers
{
    public class CallController : TwilioController
    {
        private readonly string _outboundCallerId = Settings.OutboundCallerId;
        private readonly string _agentId = Settings.AgentId;


        public ActionResult OutboundDial()
        {
            var twilioResponse = new TwilioResponse();
            var number = "";
            
            if (Request["PhoneNumber"] != null)
            {
                number = Request["PhoneNumber"];
            }

            var dialAttributes = new
            {
                callerId = _outboundCallerId,
            };

            twilioResponse.Dial(number, dialAttributes);

            return TwiML(twilioResponse);
        }

        public ActionResult RouteInboundCall()
        {
            var twilioResponse = new TwilioResponse();
            twilioResponse.Dial(new Client(_agentId));
            return TwiML(twilioResponse);
        }
    }
}
