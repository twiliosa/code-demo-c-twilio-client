﻿using System.Web.Mvc;
using Twilio;

namespace TwilioClient.Controllers
{
    public class HomeController : Controller
    {
        private readonly string _clientAppSid = Settings.ClientAppSid ;
        private readonly string _accountSid = Settings.TwilioAccountSid;
        private readonly string _authToken = Settings.TwilioAuthToken;
        private readonly string _agentId = Settings.AgentId;


        public ActionResult Index()
        {
            var capability = new TwilioCapability(_accountSid, _authToken);
            capability.AllowClientOutgoing(_clientAppSid);
            capability.AllowClientIncoming(_agentId);
            ViewBag.Token = capability.GenerateToken();

            return View();
        }
    }
}
