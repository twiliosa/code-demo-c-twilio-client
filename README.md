# Twilio Csharp Javascript Client Demo

-----

### What is Twilio Javascript Client?
Twilio Javascript Client is a means for making and receiving VoIP phone calls directly from or in the browser. This demo is a Twilio MVC implementation of the Twilio Csharp Javascript Client. In this demo, all you'll need to do is fill in a few fields in a settings file, and set a few URLs in your account portal, and your Twilio Client instance should be set to make and receive calls.

### Live Demo
Try out a live version of this demo here: [http://twilio-client-demo.azurewebsites.net/](http://twilio-client-demo.azurewebsites.net/). As with all of our Client JS demos, you will have the best quality experience if you open this in Google Chrome.

--------

# Setup

In order to get this demo running, you'll need to customize settings on your account portal and in the `Settings.cs` file. 

### Customizing endpoints on your Account Portal
Your account portal is your graphical interface for managing and testing out your applications. For this application you'll need 2 resources, a phone number and a TwiML App, and for each of the resources you'll need to set one URL to forward requests to your server.

#### Phone numbers
To buy a phone number, go to the [Buy a Number page](https://www.twilio.com/user/account/phone-numbers/available/local), then search for and buy a number. By default, you will be redirected to the number's setup page. If not, then navigate to that number's setup page by going to your [numbers page](https://www.twilio.com/user/account/phone-numbers/incoming) and clicking on the number in question. Once on the setup page, set the **Voice Request URL** to the following URL: `http://www.INSERT-YOUR-DOMAIN.com/Call/RouteInboundCall`. Be sure to replace "`INSERT-YOUR-DOMAIN`" with your actual domain name. Now, whenever your phone number receives a call, Twilio will send a request the the URL at `http://www.INSERT-YOUR-DOMAIN.com/Call/RouteInboundCall`. Here, Twilio expects to receive instructions for handling or routing the call. (NOTE: Your app will need to be deployed on the public internet in order to make or receive calls).

You now own a phone number and should receive inbound phone calls from it.

#### TwiML App
When your Twilio Client instance tries to make a phone call, it needs to look up instructions on how to make a phone call. The TwiML App is an object for telling your Twilio Client instance where to look up those instructions. To create a TwiML App, go to the [Create TwiML App page](https://www.twilio.com/user/account/apps/add). Set the Friendly Name to any name, and then set the **Voice Request URL** to the following URL: `http://www.INSERT-YOUR-DOMAIN.com/Call/OutboundDial`. Be sure to replace "`INSERT-YOUR-DOMAIN`" with your actual domain name. When your Twilio Client instance tries to make a phone call, it will first query the URL to look for instructions on how to actually make a phone call. (NOTE: Your app will need to be deployed on the public internet in order to make or receive calls). 

Now your Twilio Client instance is almost setup to make outbound calls.

### Customizing the `Settings.cs` file
The last file you'll need to customize in order to make and receive calls is the `Settings.cs` file. This file contains your account specific and sensitive information. 

```csharp
public class Settings
{
    public static string TwilioAccountSid { get { return ""; } }
    public static string TwilioAuthToken { get { return ""; } }
    public static string ClientAppSid { get { return ""; } }
    public static string OutboundCallerId { get { return ""; } }
    public static string AgentId { get { return ""; } }
}
```

Fill in the `TwilioAccountSid` and `TwilioAuthToken` properties with your own Twilio AccountSid and AuthToken found on your [Account Portal Dashboard](https://www.twilio.com/user/account). Set the `ClientAppSid` to the SID of the TwiML App you just created (go to the [TwiML Apps page](https://www.twilio.com/user/account/apps), then click on the app you just created and copy and paste the AppSid). Set the `OutboundCallerId` to the phone number you purchased earlier (Copy and paste the phone number that you just bought. If you forgot the number, search for it in the [Numbers page](https://www.twilio.com/user/account/phone-numbers/incoming)). Lastly, set the `AgentId` to any name ("bob," "sally," "berta," etc).

Your application should now be set to make and receive phone calls.

-----

## Deploying your App
You'll need to deploy your app to the public internet. Deploying to the Windows Azure Cloud Hosting Platform is probably the most common way to deploy the app. To deploy to Windows Azure, log into your Windows Azure Account portal. Go to the `Web Sites` tab, Click `+New`, click `Quick Create`, and set your App's URL. For example, "`twilio-client-demo.azuerwebsites.net`" would be a great name except that it is already taken by this app. Click on the website instance to open up the website's settings page, then click "`Download the publish profile`" to download the publish profile of your website. In Visual Studio, right click on your solution, click `Publish`, and then you will be prompted to "`Select a target publish`." Click `Import` and import the publish profile you just downloaded. Finally, click `Publish` and your app should publish.